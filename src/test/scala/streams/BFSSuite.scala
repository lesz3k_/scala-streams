package streams

import org.scalatest.FunSuite

import scala.collection.immutable.Queue

/**
  * Created by lesz3k on 06/04/17.
  */
class BFSSuite extends FunSuite {

  sealed case class Graph(rootVertex: Vertex)
  sealed case class Vertex(adjacentVertices: List[Vertex], value: String)

  def createGraph(): Graph = {
    val f = Vertex(Nil, "f")
    val e = Vertex(Nil, "e")

    val b = Vertex(List(e, f), "b")
    val c = Vertex(Nil, "c")
    val d = Vertex(Nil, "d")

    val a = Vertex(List(b, c, d), "a")
    Graph(a)
  }

  def bfs(graph: Graph, rootVertex: Vertex, goal: String): Vertex = {

    def bfsSolution(goal: String, queue: Queue[Vertex], visitedSet: Set[Vertex]): Vertex = {
      if (queue.isEmpty) Vertex(Nil, "no solution")
      else {
        val vertexWithQueue = queue.dequeue

        val current = vertexWithQueue._1
        val updatedQueue = vertexWithQueue._2

        println(s"Current: $current")

        if (current.value == goal) return current

        val vertices = current.adjacentVertices
          .filter(f => !visitedSet(f))

        bfsSolution(goal, updatedQueue.enqueue(vertices), visitedSet ++ vertices)
      }
    }

    val s = Set(rootVertex)
    val queue = Queue(rootVertex)

    bfsSolution(goal, queue, s)

  }

  test("traverse the graph down to e") {

    val graph = createGraph()
    val goal = "e"

    bfs(graph, graph.rootVertex, goal)
  }
}
