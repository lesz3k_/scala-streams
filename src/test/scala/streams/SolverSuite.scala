package streams

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by lesz3k on 30/03/17.
  */
@RunWith(classOf[JUnitRunner])
class SolverSuite extends FunSuite with StringParserTerrain with Solver {

  test("isStanding") {
    val testBlock = Block(Pos(1,1), Pos(1,1))

    assert(testBlock.isStanding === true)
  }

  test("isStanding on laying block") {
    val testBlock = Block(Pos(1,2), Pos(1,3))

    assert(testBlock.isStanding === false)
  }

  test("isLegal on start") {
    val b = Block(Pos(1,1), Pos(1,1))
    assert(b.isLegal === true)
  }

  test("isLegal outside terrain") {
    val b = Block(Pos(2,0), Pos(3,0))
    assert(b.isLegal === false)
  }

  test("test neighborsWithHistory standing") {
    val expected = Set(
      (Block(Pos(1, 2), Pos(1, 3)), List(Right, Right, Right, Right)),
      (Block(Pos(2, 1), Pos(3, 1)), List(Down, Right, Right, Right))).toStream

    val neighbors = neighborsWithHistory(Block(Pos(1, 1), Pos(1, 1)), List(Right, Right, Right))
    assert(neighbors === expected)
  }

  test("test neighborsWithHistory laying same column") {

    val testBlock = Block(Pos(2, 4), Pos(3, 4))

    val expected = Set(
      (Block(Pos(2, 3), Pos(3, 3)), List(Left, Right, Up, Left)),
      (Block(Pos(2, 5), Pos(3, 5)), List(Right, Right, Up, Left)),
      (Block(Pos(1, 4), Pos(1, 4)), List(Up, Right, Up, Left))).toStream

    val neighbors = neighborsWithHistory(testBlock, List(Right, Up, Left))
    assert(neighbors === expected)
  }

  test("newNeighborsOnly") {
    val input = Set(
      (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
      (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))
    ).toStream

    val visited = Set(Block(Pos(1,2),Pos(1,3)), Block(Pos(1,1),Pos(1,1)))

    val expected = Set(
      (Block(Pos(2,1),Pos(3,1)), List(Down,Left,Up))
    ).toStream

    assert(newNeighborsOnly(input, visited).toSet.size === 1)
    assert(newNeighborsOnly(input, visited) === expected)
  }

  test("pathsFromStart") {
    pathsFromStart
  }

  /**
    * A ASCII representation of the terrain. This field should remain
    * abstract here.
    */
  override val level: String =
    """ooo-------
      |oSoooo----
      |ooooooooo-
      |-ooooooooo
      |-----ooToo
      |------ooo-""".stripMargin

}
