package streams

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by lesz3k on 27/03/17.
  */
@RunWith(classOf[JUnitRunner])
class StringParserTerrainSuite extends FunSuite with StringParserTerrain {

  test("find starting position") {
    assert(startPos === Pos(1, 2))
  }

  test("block outside valid terrain") {
    assert(terrain(Pos(0, 0)) === false)
  }

  test("block inside valid terrain") {
    assert(terrain(Pos(2, 2)) === true)
  }



  override val level: String =
    """-------
      |--STo--
      |--ooo--
      |--ooo--
      |-------""".stripMargin
}
